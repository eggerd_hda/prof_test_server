# Professionelles Testen - Server
*5\. Semester, 2018 (Wahlpflicht)*  
*Prof. Dr. Kai Renz*

Hierbei handelt es sich um die erste Hälfte des Praktikum, in der von uns ein Server programmiert und getestet wurde, der in regelmäßigen Abständen Flugpositionsdaten von der öffentlichen API des [OpenSky Network](https://opensky-network.org/) abfragt, aufbereitet, filtert und diese über eine [REST-Schnittstelle](#rest-schnittstelle) zur Verfügung stellt.

In der [zweiten Hälfte des Praktikum](https://gitlab.com/eggerd_hda/prof_test_client) wurde dann ein entsprechender Web-Client programmiert, der die vom Server bereitgestellten Flugpositionsdaten auf einer Landkarte anzeigt.


## Behandelte Themen
- Testen im Java Umfeld mit [JUnit](https://junit.org/)
- Whitebox-Tests
- Code-Coverage mit [JaCoCo](https://www.jacoco.org/)
- Umgang mit Legacy-Code um Testbarkeit herzustellen
- Mocks mit [Mockito](https://site.mockito.org/)
- Dependency-Injection (manuell & mit [Google Guice](https://github.com/google/guice))
- Testen von REST-Schnittstellen mit [REST Assured](http://rest-assured.io/)


## Anforderungen
- In regelmäßigen Abständen sollen Flugpositionsdaten vom [OpenSky Network](https://opensky-network.org/) abgefragt werden
- Die Flugpositionsdaten sollen anhand ihrer geographischen Position und Höhe gefiltert werden können
- Flugspuren sollen berechnet und zwischengespeichert werden können
- Es soll eine REST-Schnittstelle bereitgestellt werden, die ...
    - die aufgearbeiteten Flugpositionsdaten bereitstellt
    - es ermöglicht einen Filter (Position & Höhe) für die aktuelle Session zu setzten
    - den Filter zurückgibt, der für die aktuelle Session gesetzt wurde
- Selbst implementierte Funktionen sollten möglichst vollständig (Zweigüberdeckung) getestet sein, mit Hilfe von...
    - Unit-Tests
    - Dependency-Injection
    - Mocks/Stubs
    - etc.


## Endergebnis
- Alle von uns implementierten Klassen haben eine Testabdeckung von 100%
![Code Coverage](https://gitlab.com/eggerd_hda/prof_test_server/-/raw/assets/coverage.png)
- Aus Zeitmangel wurde die Anforderung zum Berechnen und Zwischenspeichern von Flugspuren vom Professor verworfen
- Aufgrund geänderter Anforderungen an den [Web-Client](https://gitlab.com/eggerd_hda/prof_test_client) wurde lediglich das Setzten des geographischen Filters, nicht aber des Höhenfilters, in die [REST-Schnittstelle](#rest-schnittstelle) implementiert
- Unsere Implementierung wurde als Grundlage für die bis dato noch nicht existierende Musterlösung verwendet, was uns natürlich sehr gefreut hat


## Demo

#### Installation

##### Docker
Der einfachste Weg ist die Verwendung von [Docker](https://www.docker.com/), da der bereitgestellte [Container](https://hub.docker.com/r/eggerd/hda-prof-test) bereits alle Abhängigkeiten mit sich bring und sowohl den Server als auch den [Web-Client](https://gitlab.com/eggerd_hda/prof_test_client) bereitstellt.

Über den nachfolgenden Befehl kann der Container heruntergeladen und gestartet werden.
```sh
docker run -it -p 8080:8080 -p 8383:8383 eggerd/hda-prof-test
```
Sobald im Terminal die Nachricht `Starting up http-server` erscheint, ist der Startvorgang abgeschlossen und die [REST-Schnittstelle](#rest-schnittstelle) des Servers ist nun über einen beliebigen Browser unter `http://localhost:8080` zu erreichen.


##### Manuell
Alternativ kann der Server auch manuell mit Hilfe von [Maven](https://maven.apache.org/) kompiliert werden. Zum Ausführen des Servers muss zudem [Java](https://www.java.com/de/) 1.8 oder höher installiert sein.

Durch das Ausführen des nachfolgenden Befehls im Hauptverzeichnis des Repositories wird der Server kompiliert und anschließend gestartet.
```sh
mvn install && java -jar ./target/prof_test_server-1.0.0-SNAPSHOT-fat.jar
```
Sobald im Terminal die Nachricht `BUILD SUCCESS` erscheint, ist der Startvorgang abgeschlossen und die [REST-Schnittstelle](#rest-schnittstelle) des Servers ist nun über einen beliebigen Browser unter `http://localhost:8080` zu erreichen.


#### REST-Schnittstelle
Es wird empfohlen Firefox zur Kommunikation mit der REST-Schnittstelle zu verwenden, da Firefox die in JSON formatierten Antworten des Servers automatisch in einem für Menschen lesbaren Format anzeigt. Zum Senden von POST-Requests kann unter Firefox die Erweiterung "[RESTED](https://addons.mozilla.org/de/firefox/addon/rested/)" verwendet werden.

Die Schnittstelle bietet folgende Funktionen:

| Pfad | Art | Beschreibung |
| --- | --- | --- |
| `/flights` | GET | Liefert eine Liste mit Flugpositionsdaten aller Flüge die sich innerhalb des definierten Filters befinden, sowie einen UNIX-Timestamp der letzten Aktualisierung |
| `/filter` | GET | Gibt die aktuellen geographischen Koordinaten des Filters zurück |
| `/filter` | POST | Setzt die geographischen Koordinaten des Filter. Erwartet einen JSON-String mit den gewünschten Koordinaten als Payload. Format: `{lat1: [double], lon1: [double], lat2: [double], lon2: [double]}` |


## Beteiligte Personen
- [Dustin Eckhardt](https://gitlab.com/eggerd)
- Johannes Reichard


## Verwendete Software
- [Java](https://www.java.com/de/) (1.8)
- [JUnit](https://junit.org/) (3.8.1)
- [JaCoCo](https://www.jacoco.org/) (0.8.1)
- [Maven](https://maven.apache.org/) (3.1)
- [Mockito](https://site.mockito.org/) (2.7.0)
- [Google Guice](https://github.com/google/guice) (4.1.0)
- [REST Assured](http://rest-assured.io/) (3.0.0)
- [Vert.x](https://vertx.io/) (3.5.1)