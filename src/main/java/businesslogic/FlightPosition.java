package businesslogic;

/**
 * Represents the position (latitude, longitude & height) of a flight
 */
public class FlightPosition implements GeographicPosition {

    private final float latitude;
    private final float longitude;
    private final float height;
    private final float heading;
    private FlightPosition previous;

    /**
     * Set position data
     *
     * @param latitude WGS-84 latitude in decimal degrees (Index 6)
     * @param longitude WGS-84 longitude in decimal degrees (Index 5)
     * @param height Geometric altitude in meters (Index 7)
     * @param heading Heading in decimal degrees clockwise from north
     */
    public FlightPosition(float latitude, float longitude, float height, float heading) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.height = height;
        this.heading = heading;
    }

    /**
     * Sets the previous FlightPosition
     *
     * @param previous the previous position
     * @return The object
     */
    public FlightPosition setPrevious(FlightPosition previous) {
        this.previous = previous;
        return this;
    }

    /**
     * @return The previous FlightPosition
     */
    public FlightPosition previous() {
        return previous;
    }

    @Override
    public float getLatitude() {
        return latitude;
    }

    @Override
    public float getLongitude() {
        return longitude;
    }

    public float getHeight() {
        return height;
    }

    public float getHeading() {
        return heading;
    }
}
