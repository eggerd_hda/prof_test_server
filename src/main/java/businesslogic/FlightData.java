package businesslogic;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class FlightData {

    /**
     * Key = icao24 address of the flight / Value = a flight
     */
    private Map<String, Flight> map = new HashMap();
    private int timestamp;

    /**
     * Default constructor
     */
    public FlightData() {

    }

    /**
     * Creates a map that contains all flights out of a JSON string
     *
     * @param jsonString the Json string that is returned by the API
     * @throws Exception Unable to create map of flight data - no flight states
     * found!
     */
    public FlightData(String jsonString) throws Exception {
        JsonObject data = new JsonObject(jsonString);

        if (data.containsKey("states") && data.containsKey("time")) {
            timestamp = data.getInteger("time");
            JsonArray states = data.getJsonArray("states");
            for (int i = 0; i < states.size(); i++) {
                JsonArray entry = states.getJsonArray(i);
                FlightPosition position = FlightDataProcessing.extractPosition(entry);
                if (position != null) {
                    map.put(entry.getString(0), new Flight(entry.getString(0), entry.getString(1), position, entry.getInteger(3)));
                }
            }
        } else {
            throw new Exception("Unable to create map of flight data!");
        }
    }

    /**
     * Converts the list of flights into a JSON string that contains the
     * following information for each flight: latitude, longitude, height,
     * icao24, callsing, last update timestmap
     *
     * @return A JSON string representing the FlightData
     */
    public String toJson() {
        JsonObject json = new JsonObject();
        json.put("time", timestamp);

        JsonArray array = new JsonArray();
        for (Map.Entry<String, Flight> entry : map.entrySet()) {
            JsonObject flight = new JsonObject();
            flight.put("latitude", entry.getValue().position().getLatitude());
            flight.put("longitude", entry.getValue().position().getLongitude());
            flight.put("height", entry.getValue().position().getHeight());
            flight.put("icao24", entry.getValue().getIcao24());
            flight.put("callsign", entry.getValue().getCallsign());
            flight.put("heading", entry.getValue().position().getHeading());
            flight.put("lastUpdate", entry.getValue().getLastUpdateTime());

            array.add(flight);
        }
        json.put("flights", array);

        return json.toString();
    }

    /**
     * Add a flight to the map
     *
     * @param icao24 the icao24 address of the flight
     * @param flight the Flight object representing the flight
     * @return The Object
     */
    public FlightData addFlight(String icao24, Flight flight) {
        map.put(icao24, flight);
        return this;
    }

    /**
     * Set the time this flight data has been generated
     *
     * @param timestamp unix timestamp of the time this data has been generated
     * @return The Object
     */
    public FlightData setTimestamp(int timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    /**
     * Get a flight by its icao24 address
     *
     * @param icao24 the icao24 address of the flight to return
     * @return A Flight object or NULL, if the requested flight does not exist
     */
    public Flight getFlight(String icao24) {
        return map.get(icao24);
    }

    /**
     * Check if a flight with a certain icao24 address exists
     *
     * @param icao24 the icao24 address of the flight to search
     * @return Will be TRUE, if the a flight with this icao24 address exists
     */
    public boolean flightExists(String icao24) {
        return map.containsKey(icao24);
    }

    /**
     * @return The amount of flights
     */
    public int amountFlights() {
        return map.size();
    }

    /**
     * @return The unix timestamp of the time this data has been generated
     */
    public int timestamp() {
        return timestamp;
    }

    /**
     * @return The entrySet of the internally used Map
     */
    public Set<Entry<String, Flight>> entrySet() {
        return map.entrySet();
    }
}
