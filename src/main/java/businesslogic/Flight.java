package businesslogic;

public class Flight {

    private final String icao24;
    private final String callsign;
    private final int lastUpdate;
    private final FlightPosition position;

    /**
     * Creates a new flight
     *
     * @param icao24 unique ICAO 24-bit address of the transponder in hex string
     * representation
     * @param callsign callsign of the vehicle (8 chars)
     * @param position a flights position
     * @param lastUpdate unix timestamp for the last position update
     */
    public Flight(String icao24, String callsign, FlightPosition position, int lastUpdate) {
        this.icao24 = icao24;
        this.callsign = callsign;
        this.position = position;
        this.lastUpdate = lastUpdate;
    }

    /**
     * Sets the previous FlightPosition
     *
     * @param position the previous position
     * @return The object
     */
    public Flight setPrevious(FlightPosition position) {
        this.position.setPrevious(position);
        return this;
    }

    /**
     * @return The position object of the flight
     */
    public FlightPosition position() {
        return position;
    }

    /**
     * @return The icao24 address of the flight
     */
    public String getIcao24() {
        return icao24;
    }

    /**
     * @return The callsign of the flight
     */
    public String getCallsign() {
        return callsign;
    }

    /**
     * @return Unix timestamp for the last position update
     */
    public int getLastUpdateTime() {
        return lastUpdate;
    }
}
