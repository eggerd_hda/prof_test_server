package businesslogic;

public interface GeographicPosition {

    float getLatitude();

    float getLongitude();
}
