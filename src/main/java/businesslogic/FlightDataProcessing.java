package businesslogic;

import io.vertx.core.json.JsonArray;
import java.util.Map;

/* The states property is a two-dimensional array. Each row represents a state vector and contains the following fields:
 * Pos  Property 	Type        Description
 * 0 	icao24          string      Unique ICAO 24-bit address of the transponder in hex string representation.
 * 1 	callsign        string      Callsign of the vehicle (8 chars). Can be null if no callsign has been received.
 * 2 	origin_country  string      Country name inferred from the ICAO 24-bit address.
 * 3 	time_position   int         Unix timestamp (seconds) for the last position update. Can be null if no position report was received by OpenSky within the past 15s.
 * 4 	last_contact    int         Unix timestamp (seconds) for the last update in general. This field is updated for any new, valid message received from the transponder.
 * 5 	longitude       float       WGS-84 longitude in decimal degrees. Can be null.
 * 6 	latitude        float       WGS-84 latitude in decimal degrees. Can be null.
 * 7 	geo_altitude    float       Geometric altitude in meters. Can be null.
 * 8 	on_ground       boolean     Boolean value which indicates if the position was retrieved from a surface position report.
 * 9 	velocity        float       Velocity over ground in m/s. Can be null.
 * 10 	heading         float       Heading in decimal degrees clockwise from north (i.e. north=0°). Can be null.
 * 11 	vertical_rate   float       Vertical rate in m/s. A positive value indicates that the airplane is climbing, a negative value indicates that it descends. Can be null.
 * 12 	sensors         int[]       IDs of the receivers which contributed to this state vector. Is null if no filtering for sensor was used in the request.
 * 13 	baro_altitude   float       Barometric altitude in meters. Can be null.
 * 14 	squawk          string      The transponder code aka Squawk. Can be null.
 * 15 	spi             boolean     Whether flight status indicates special purpose indicator.
 * 16 	position_source int         Origin of this state’s position: 0 = ADS-B, 1 = ASTERIX, 2 = MLAT
 */
public class FlightDataProcessing {

    /**
     * Determines if a flights position is within a specified region
     *
     * @param filter coordinates of the region the flight has to be in
     * @param flightPosition a flights position
     * @return Will be TRUE, if the flight is within the region
     */
    public static boolean filterGeographicPosition(FlightFilterValues filter, FlightPosition flightPosition) {
        if (flightPosition.getLatitude() >= filter.getLatitudes()[0] && flightPosition.getLatitude() <= filter.getLatitudes()[1]) {
            if (flightPosition.getLongitude() >= filter.getLongitudes()[0] && flightPosition.getLongitude() <= filter.getLongitudes()[1]) {
                return true;
            }
        }

        return false;
    }

    /**
     * Determines if a flights height is within the specified range
     *
     * @param filter height range the flight has to be in
     * @param flightPosition a flights position
     * @return Will be TRUE, if the flight is within the height range
     */
    public static boolean filterFlightHeight(FlightFilterValues filter, FlightPosition flightPosition) {
        return flightPosition.getHeight() >= filter.getHeights()[0] && flightPosition.getHeight() <= filter.getHeights()[1];
    }

    /**
     * Extracts a flights position out of its vector
     *
     * @param flightData the vector of a flight
     * @return A flights position (latitude, longitude & height) or NULL if the
     * position is invalid
     */
    public static FlightPosition extractPosition(JsonArray flightData) {
        if (!flightData.hasNull(5) && !flightData.hasNull(6) && !flightData.hasNull(7) && !flightData.hasNull(3) && !flightData.hasNull(10)) {
            float latitude = flightData.getFloat(6);
            float longitude = flightData.getFloat(5);
            float height = flightData.getFloat(7);
            float heading = flightData.getFloat(10);

            if (latitude >= -90f && latitude <= 90f && longitude >= -180f && longitude <= 180f) {
                if (height >= 0 && heading >= 0 && heading <= 360) {
                    return new FlightPosition(latitude, longitude, height, heading);
                }
            }
        }

        return null;
    }

    /**
     * Sets the previous position for all flights that are contained in both
     * FlightData maps
     *
     * @param oldData the old FlightData from the previous poll
     * @param newData the new FlightData from the latest poll
     */
    public static void setPreviousPositions(FlightData oldData, FlightData newData) {
        for (Map.Entry<String, Flight> entry : oldData.entrySet()) {
            if (newData.flightExists(entry.getKey())) {
                newData.getFlight(entry.getKey()).setPrevious(entry.getValue().position());
            }
        }
    }
}
