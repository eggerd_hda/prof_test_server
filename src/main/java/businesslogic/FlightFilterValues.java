package businesslogic;

import io.vertx.core.json.JsonObject;
import java.util.Arrays;

/**
 * Small helper class for FlightDataProcessing.filterGeographicPosition()
 */
public class FlightFilterValues {

    private float[] latitude = {-90, 90};
    private float[] longitude = {-180, 180};
    private float[] height = {0, 20000};

    /**
     * Set the latitude & longitude coordinates for the filter region
     *
     * @param latMin should be the smallest latitude of the filter region
     * @param latMax should be the biggest latitude of the filter region
     * @param lonMin should be the smallest longitude of the filter region
     * @param lonMax should be the biggest longitude of the filter region
     * @return The object
     */
    public FlightFilterValues setRegion(float latMin, float latMax, float lonMin, float lonMax) {
        latitude = new float[]{latMin, latMax};
        longitude = new float[]{lonMin, lonMax};
        return this;
    }

    /**
     * Set the height range for the filter
     *
     * @param min minimum flight height
     * @param max maximum flight height
     * @return The object
     */
    public FlightFilterValues setHeight(float min, float max) {
        height = new float[]{min, max};
        return this;
    }

    /**
     * Converts the filter region into a JSON string
     *
     * @return A JSON string representing the filter region
     */
    public String regionToJson() {
        JsonObject json = new JsonObject();
        json.put("lat1", getLatitudes()[0]);
        json.put("lon1", getLongitudes()[0]);
        json.put("lat2", getLatitudes()[1]);
        json.put("lon2", getLongitudes()[1]);

        return json.toString();
    }

    /**
     * @return A sorted array containing the two latitude coordinates
     */
    public float[] getLatitudes() {
        Arrays.sort(latitude);
        return latitude;
    }

    /**
     * @return A sorted array containing the two longitude coordinates
     */
    public float[] getLongitudes() {
        Arrays.sort(longitude);
        return longitude;
    }

    /**
     * @return A sorted array containing the minimum and maximum height
     */
    public float[] getHeights() {
        Arrays.sort(height);
        return height;
    }
}
