package server;

import businesslogic.Flight;
import businesslogic.FlightData;
import businesslogic.FlightDataProcessing;
import businesslogic.FlightFilterValues;
import com.google.inject.Inject;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Cookie;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CookieHandler;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import network.NetworkAccess;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import storage.FlightDataStorage;

public class Server extends AbstractVerticle {

    private int port = 8080;
    private Map<String, FlightFilterValues> filter = new HashMap();
    private static Logger logger = LoggerFactory.getLogger(Server.class);
    final public String openskyURL = "https://opensky-network.org/api/states/all";
    public FlightData previousFlightData = new FlightData();
    public FlightData flightData = new FlightData();

    @Inject
    private NetworkAccess na;

    @Inject
    private FlightDataProcessing flightDataProcessing;

    @Inject
    private FlightDataStorage flightDataStorage;

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        Router router = Router.router(vertx);

        // this will handle the Body in POST Requests ....
        router.route().handler(BodyHandler.create());
        // this will detect cookies ....
        router.route().handler(CookieHandler.create());

        router.get("/flights").handler(getFlights);
        router.post("/filter").handler(setFilter);
        router.get("/filter").handler(getFilter);
        router.get("/").handler(routingContext -> {
            HttpServerResponse response = routingContext.response();
            response.putHeader("Access-Control-Allow-Origin", "*");
            response.putHeader("content-type", "application/json");
            response.end(new JsonObject().put("greeting", "Hello Aviation World!").toBuffer());
        });

        vertx.createHttpServer().requestHandler(router::accept).listen(port, (done) -> {
            if (done.succeeded()) {
                startFuture.complete();
            } else {
                startFuture.fail(done.cause());
            }
        });

        // enables polling of data every 5 seconds (5000 ms)
        //na.activateRepeatedDataCollection(this,"https://agileprototype.de/tracks/hello",5000);
    }

    /**
     * Decodes the JSON, extracts all flights and their position and then
     * applies the filter and sets previous positions
     *
     * @param result the JSON string containing the flight states
     */
    public void processResult(String result) {
        try {
            FlightData newflightData = new FlightData(result);

            if (newflightData.timestamp() <= flightData.timestamp()) {
                logger.info("Time hasn't changed!");
                return;
            }

            previousFlightData = flightData;
            flightData = newflightData;
            FlightDataProcessing.setPreviousPositions(previousFlightData, flightData);

            logger.info("Processed " + flightData.amountFlights() + " flights (" + flightData.timestamp() + ")");
        } catch (Exception e) {
            logger.info("Exception has been thrown: " + e.getMessage());
        }
    }

    /**
     * Handler for getting all current flights
     */
    Handler<RoutingContext> getFlights = routingContext -> {
        HttpServerResponse response = routingContext.response();
        response.putHeader("Access-Control-Allow-Origin", "*");
        response.putHeader("Content-Type", "application/json");
        response.putHeader("Cache-Control", "max-age=1");

        processResult(getData());

        logger.info("SessionID: " + getSessionID(routingContext));
        FlightData filteredData = new FlightData();
        filteredData.setTimestamp(flightData.timestamp());
        for (Map.Entry<String, Flight> entry : flightData.entrySet()) {
            if (FlightDataProcessing.filterGeographicPosition(filter.get(getSessionID(routingContext)), entry.getValue().position())) {
                filteredData.addFlight(entry.getKey(), entry.getValue());
            }
        }

        response.end(filteredData.toJson());
        logger.info(filteredData.amountFlights() + " flights inside filter region");
    };

    /**
     * Handler for setting a filter region
     */
    Handler<RoutingContext> setFilter = routingContext -> {
        HttpServerResponse response = routingContext.response();
        response.putHeader("Access-Control-Allow-Origin", "*");
        response.putHeader("Content-Type", "application/json");

        JsonObject jsonBody = new JsonObject();
        Buffer body = routingContext.getBody();
        if (body.length() > 0) {
            try {
                jsonBody = new JsonObject(body);
            } catch (DecodeException de) {
                logger.warn("Could not decode filter: " + body.toString());
            }
        }

        if (jsonBody.containsKey("lat1") && jsonBody.containsKey("lat2")
                && jsonBody.containsKey("lon1") && jsonBody.containsKey("lon2")) {

            FlightFilterValues userFilter = filter.get(getSessionID(routingContext));
            userFilter.setRegion(jsonBody.getFloat("lat1"), jsonBody.getFloat("lat2"), jsonBody.getFloat("lon1"), jsonBody.getFloat("lon2"));
            response.end(userFilter.regionToJson());
            return;
        }

        response.end("{}");
    };

    /**
     * Handler for getting the current filter region
     */
    Handler<RoutingContext> getFilter = routingContext -> {
        HttpServerResponse response = routingContext.response();
        response.putHeader("Access-Control-Allow-Origin", "*");
        response.putHeader("Content-Type", "application/json");
        response.putHeader("Cache-Control", "max-age=1");

        response.end(filter.get(getSessionID(routingContext)).regionToJson());
    };

    /**
     * Returns the ID for the current session. If no session ID exists, a new
     * one will be generated & the users data (filter) will be initialized
     *
     * @param routingContext routing context of the current request
     * @return The users session ID
     */
    public String getSessionID(RoutingContext routingContext) {
        Cookie sessionCookie = routingContext.getCookie("JSESSIONID");
        if (sessionCookie != null && filter.containsKey(sessionCookie.getValue())) {
            return sessionCookie.getValue();
        }

        // generating new session ID & initializing filter
        String sessionId = UUID.randomUUID().toString();
        routingContext.response().putHeader("Set-Cookie", "JSESSIONID=" + sessionId);
        filter.put(sessionId, new FlightFilterValues());
        return sessionId;
    }

    public String getData() {
        return na.getWebsiteResult(openskyURL);
    }

    public Server setPort(int port) {
        this.port = port;
        return this;
    }

    public void resetData() {
        previousFlightData = new FlightData();
        flightData = new FlightData();
        filter = new HashMap();
    }

    @Override
    public void stop() {
        na.shutdown();
    }
}
