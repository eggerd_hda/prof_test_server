package server;

import com.google.inject.Guice;
import com.google.inject.Injector;
import io.vertx.core.Vertx;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

    static Logger logger = LoggerFactory.getLogger(Server.class);

    public static void main(String[] args) {
        logger.debug("Hello World from MAIN");

        Injector injector = Guice.createInjector();
        Server server = injector.getInstance(Server.class);

        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(server);

        /*try {
            Thread.sleep(11000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
        //server.stop();
    }
}
