package network;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import server.Server;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class NetworkAccess {

    static Logger logger = LoggerFactory.getLogger(NetworkAccess.class);

    /**
     * Reads the content from a Http-Server given the @param urlString Shows an
     * error in the internal log if there is a problem. Returns an empty string
     * in this case.
     *
     * @param urlString
     * @return
     */
    public String getWebsiteResult(String urlString) {

        StringBuilder buf = null;
        try {
            URL url = new URL(urlString);
            logger.info("opening connection");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setConnectTimeout(10000); // set the timeout to 10 seconds
            con.setRequestMethod("GET");

            buf = new StringBuilder();
            InputStreamReader br = new InputStreamReader(con.getInputStream());
            logger.info("reading bytes");
            int readByte = -1;
            while ((readByte = br.read()) != -1) {
                buf.append((char) readByte);
            }
            con.disconnect();
            logger.info("returning content");
            return buf.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        } finally {

        }
    }

    ScheduledExecutorService scheduledExecutorService;

    public ScheduledExecutorService activateRepeatedDataCollection(Server server, String urlString, long refreshRateInMs) {

        // do the repeating call
        scheduledExecutorService = Executors.newScheduledThreadPool(1);
        scheduledExecutorService.scheduleAtFixedRate(() -> {

            String result = getWebsiteResult("https://agileprototype.de/tracks/hello");
            // do something with the result
            server.processResult(result);

        }, 0, refreshRateInMs, TimeUnit.MILLISECONDS);

        return scheduledExecutorService;
    }

    public void shutdown() {
        if (scheduledExecutorService != null) {
            scheduledExecutorService.shutdown();
        }
    }
}
