package server;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import io.vertx.core.Vertx;
import java.util.concurrent.atomic.AtomicBoolean;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;
import io.restassured.filter.session.SessionFilter;
import io.restassured.response.Response;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import java.io.BufferedReader;
import java.io.FileReader;
import java.net.MalformedURLException;
import network.NetworkAccess;
import static org.hamcrest.Matchers.is;
import org.junit.jupiter.api.AfterAll;
import static org.junit.jupiter.api.Assertions.fail;
import org.junit.jupiter.api.BeforeEach;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ServerRestTest {

    static Server server;
    static int port = 13641;
    static final String serverUrl = "http://localhost:13641";
    static Vertx vertx;
    static Injector injector;
    static NetworkAccess mockNetworkAccess;
    static String websiteResult;
    static boolean returnEmptyWebsiteResult;

    @BeforeAll
    static void setUpTestServer() {
        try {
            BufferedReader br1 = new BufferedReader(new FileReader("src/test/resources/flightTestData1523701890.json"));
            websiteResult = br1.readLine();

            mockNetworkAccess = mock(NetworkAccess.class);
            when(mockNetworkAccess.getWebsiteResult(anyString())).then((invocation) -> {
                return returnEmptyWebsiteResult ? "" : websiteResult;
            });

            injector = Guice.createInjector(new AbstractModule() {
                @Override
                protected void configure() {
                    bind(NetworkAccess.class).toInstance(mockNetworkAccess);
                }
            });
        } catch (Exception e) {
            fail("Exception thrown during test: " + e.getMessage());
        }

        // setting up a test server that is deploied on a test port
        server = injector.getInstance(Server.class);
        server.setPort(port);

        AtomicBoolean done = new AtomicBoolean(false);
        vertx = Vertx.vertx();
        vertx.deployVerticle(server, deployResult -> {
            if (deployResult.succeeded()) {
                done.set(true);
            } else {
                fail("Could not deploy test server!");
            }
        });

        // wait for the server to be deployed
        long counter = 500;
        while (!done.get() && (counter--) > 0) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if (!done.get()) {
            fail("Could not deploy test server!");
        }
    }

    @BeforeEach
    void resetWebsiteResultFlag() {
        returnEmptyWebsiteResult = false;
        server.resetData();
    }

    @AfterAll
    static void tearDownAfterAll() {
        vertx.close();
    }

    @Test
    void testStartTwoServersOnSamePort() throws MalformedURLException {

        int otherPort = 26789;
        Server s1 = new Server().setPort(otherPort);
        Server s2 = new Server().setPort(otherPort);

        /**
         * in order to test this, we have to start TWO Vertx-Instances. If the
         * server is deployed on the same vertx instance, it will just "share"
         * the already used port
         */
        Vertx vertx1 = Vertx.vertx();
        Vertx vertx2 = Vertx.vertx();
        AtomicBoolean done = new AtomicBoolean(false);

        vertx1.deployVerticle(s1, doneS1 -> {
            if (doneS1.succeeded()) {
                vertx2.deployVerticle(s2, doneS2 -> {
                    if (doneS2.succeeded()) {
                        fail("Started multiple servers on the same port!");
                    }
                    done.set(true);
                });
            }
        });

        // wait for the server to be deployed ....
        long counter = 500;
        while (!done.get() && (counter--) > 0) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if (!done.get()) {
            fail("Could not deploy test server!");
        }
    }

    @Test
    public void testGreating() {
        get(serverUrl + "/").then().body("greeting", is("Hello Aviation World!"));
    }

    @Test
    public void testGetFlights() {
        try {
            String response = get(serverUrl + "/flights").asString();
            JsonObject data = new JsonObject(response);

            // Checking if the primary keys exist in the response
            assertTrue(data.containsKey("time"));
            assertTrue(data.containsKey("flights"));

            JsonArray flights = data.getJsonArray("flights");
            assertEquals(3950, flights.size());

            // Validating first flight
            JsonObject f1 = flights.getJsonObject(0);
            assertEquals(57.5617, f1.getFloat("latitude"), 0.00001);
            assertEquals(35.2611, f1.getFloat("longitude"), 0.00001);
            assertEquals(10683.24, f1.getFloat("height"), 0.001);
            assertEquals("4240eb", f1.getString("icao24"));
            assertEquals("UTA370  ", f1.getString("callsign"));
            assertEquals(134.53, f1.getFloat("heading"), 0.001);
            assertEquals(1523701890, (int) f1.getInteger("lastUpdate"));

            // Validating last flight
            JsonObject f2 = flights.getJsonObject(3949);
            assertEquals(36.2614, f2.getFloat("latitude"), 0.00001);
            assertEquals(126.9899, f2.getFloat("longitude"), 0.00001);
            assertEquals(5151.12, f2.getFloat("height"), 0.001);
            assertEquals("7808f8", f2.getString("icao24"));
            assertEquals("B8251   ", f2.getString("callsign"));
            assertEquals(5.11, f2.getFloat("heading"), 0.001);
            assertEquals(1523701888, (int) f2.getInteger("lastUpdate"));
        } catch (Exception e) {
            fail("Exception thrown during test: " + e.getMessage());
        }
    }

    @Test
    public void testGetFlightsEmpty() {
        try {
            /**
             * By toggling this flag, an empty OpenSky response is simulated. In
             * this case, our server should responde with the last available
             * flight data
             */
            get(serverUrl + "/flights"); // but first, fetch data at least on time
            returnEmptyWebsiteResult = true;

            String response = get(serverUrl + "/flights").asString();
            JsonObject data = new JsonObject(response);

            // Checking if the primary keys exist in the response
            assertTrue(data.containsKey("time"));
            assertTrue(data.containsKey("flights"));

            JsonArray flights = data.getJsonArray("flights");
            assertEquals(3950, flights.size());

            // Validating first flight
            JsonObject f1 = flights.getJsonObject(0);
            assertEquals(57.5617, f1.getFloat("latitude"), 0.00001);
            assertEquals(35.2611, f1.getFloat("longitude"), 0.00001);
            assertEquals(10683.24, f1.getFloat("height"), 0.001);
            assertEquals("4240eb", f1.getString("icao24"));
            assertEquals("UTA370  ", f1.getString("callsign"));
            assertEquals(134.53, f1.getFloat("heading"), 0.001);
            assertEquals(1523701890, (int) f1.getInteger("lastUpdate"));

            // Validating last flight
            JsonObject f2 = flights.getJsonObject(3949);
            assertEquals(36.2614, f2.getFloat("latitude"), 0.00001);
            assertEquals(126.9899, f2.getFloat("longitude"), 0.00001);
            assertEquals(5151.12, f2.getFloat("height"), 0.001);
            assertEquals("7808f8", f2.getString("icao24"));
            assertEquals("B8251   ", f2.getString("callsign"));
            assertEquals(5.11, f2.getFloat("heading"), 0.001);
            assertEquals(1523701888, (int) f2.getInteger("lastUpdate"));
        } catch (Exception e) {
            fail("Exception thrown during test: " + e.getMessage());
        }
    }

    @Test
    public void testGetFlightsFiltered() {
        try {
            SessionFilter sessionFilter = new SessionFilter();

            String filterString = "{\"lat1\":47.523,\"lon1\":4.3,\"lat2\":52.1423,\"lon2\":9.45133}";
            given().filter(sessionFilter).body(filterString).post(serverUrl + "/filter").then().body(is(filterString));

            String response = given().filter(sessionFilter).get(serverUrl + "/flights").body().asString();
            JsonObject data = new JsonObject(response);

            // Checking if the primary keys exist in the response
            assertTrue(data.containsKey("time"));
            assertTrue(data.containsKey("flights"));

            JsonArray flights = data.getJsonArray("flights");
            assertEquals(190, flights.size());

            // Validating first flight
            JsonObject f1 = flights.getJsonObject(0);
            assertEquals(50.1626, f1.getFloat("latitude"), 0.00001);
            assertEquals(5.2905, f1.getFloat("longitude"), 0.00001);
            assertEquals(9144.0, f1.getFloat("height"), 0.001);
            assertEquals("3c56ef", f1.getString("icao24"));
            assertEquals("EWG8VM  ", f1.getString("callsign"));
            assertEquals(51.97, f1.getFloat("heading"), 0.001);
            assertEquals(1523701889, (int) f1.getInteger("lastUpdate"));

            // Validating last flight
            JsonObject f2 = flights.getJsonObject(189);
            assertEquals(51.5804, f2.getFloat("latitude"), 0.00001);
            assertEquals(5.2585, f2.getFloat("longitude"), 0.00001);
            assertEquals(8161.02, f2.getFloat("height"), 0.001);
            assertEquals("4841d8", f2.getString("icao24"));
            assertEquals("TRA19B  ", f2.getString("callsign"));
            assertEquals(160.09, f2.getFloat("heading"), 0.001);
            assertEquals(1523701890, (int) f2.getInteger("lastUpdate"));
        } catch (Exception e) {
            fail("Exception thrown during test: " + e.getMessage());
        }
    }

    @Test
    void testSetFilterGetFilter() {
        SessionFilter sessionFilter = new SessionFilter();

        Response newSession = given().filter(sessionFilter).get(serverUrl + "/filter");
        assertTrue((newSession.getCookie("JSESSIONID") != null));

        // Checking if default filter has been returned
        assertTrue(validateDefaultFilterRegion(newSession.body().asString()));

        // Posted filter should be returned, if it was successfully set
        String filterString = "{\"lat1\":47.0,\"lon1\":3.5,\"lat2\":57.0,\"lon2\":15.5}";
        given().filter(sessionFilter).body(filterString).post(serverUrl + "/filter").then().body(is(filterString));

        // Getter should return the newly set filter
        given().filter(sessionFilter).get(serverUrl + "/filter").then().body(is(filterString));

        // Posting an empty body should return a empty JSON string
        given().filter(sessionFilter).post(serverUrl + "/filter").then().body(is("{}"));

        // Posting an invalid body should return a empty JSON string
        given().filter(sessionFilter).body("not json").post(serverUrl + "/filter").then().body(is("{}"));

        // Getter should still return the old filter, since last two post requests failed
        given().filter(sessionFilter).get(serverUrl + "/filter").then().body(is(filterString));

        // Default filter should be returned, if a invalid session ID is submitted
        Response invalidSession = given().filter(sessionFilter).cookie("JSESSIONID", "invalid").get(serverUrl + "/filter");
        assertTrue(validateDefaultFilterRegion(invalidSession.body().asString()));
    }

    /**
     * Validates if the passed JSON string is the representation of the default
     * filter
     *
     * @param responseString the JSON string to validate as default filter
     * @return Will be TRUE, if the string contains latitude & longitude
     * coordinates of the default filter region
     */
    boolean validateDefaultFilterRegion(String responseString) {
        try {
            JsonObject df = new JsonObject(responseString);
            if (df.containsKey("lat1") && df.containsKey("lat2") && df.containsKey("lon1") && df.containsKey("lon2")) {
                if (-90 == df.getFloat("lat1") && 90 == df.getFloat("lat2") && -180 == df.getFloat("lon1") && 180 == df.getFloat("lon2")) {
                    return true;
                }
            }
        } catch (Exception e) {
            fail("Exception thrown during test: " + e.getMessage());
        }

        return false;
    }
}
