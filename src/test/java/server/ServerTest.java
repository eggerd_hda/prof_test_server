package server;

import businesslogic.FlightPosition;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import io.vertx.core.json.JsonObject;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import network.NetworkAccess;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ServerTest {

    Injector injector;
    Server server;
    NetworkAccess mockNetworkAccess;

    String websiteResult;
    List<String> getDataUrlStrings;

    @BeforeEach
    void setUpNetworkAccessMock() {
        try {
            BufferedReader br1 = new BufferedReader(new FileReader("src/test/resources/flightTestData1523701890.json"));
            websiteResult = br1.readLine();

            mockNetworkAccess = mock(NetworkAccess.class);
            when(mockNetworkAccess.getWebsiteResult(anyString())).then((invocation) -> {
                getDataUrlStrings.add(invocation.getArgument(0));
                return websiteResult;
            });

            injector = Guice.createInjector(new AbstractModule() {
                @Override
                protected void configure() {
                    bind(NetworkAccess.class).toInstance(mockNetworkAccess);
                }
            });
        } catch (Exception e) {
            fail("Exception thrown during test: " + e.getMessage());
        }

        getDataUrlStrings = new ArrayList<>();
        server = injector.getInstance(Server.class);
    }

    @Test
    void testGetData() {
        try {
            String returnedData = server.getData();

            // getWebsiteResult() should have been called once by getData()
            verify(mockNetworkAccess, times(1)).getWebsiteResult(anyString());

            // mocked getWebsiteResult() should have saved the passed URL
            assertEquals(1, getDataUrlStrings.size());
            assertEquals(server.openskyURL, getDataUrlStrings.get(0));

            // getData() should return the text that is returned by the mock
            assertEquals(websiteResult, returnedData);

            // Check the returned JSON (superficially)
            JsonObject jsonData = new JsonObject(returnedData);
            assertTrue(jsonData.containsKey("time"));
            assertTrue(jsonData.containsKey("states"));
            assertEquals(4219, jsonData.getJsonArray("states").size());
        } catch (Exception e) {
            fail("Exception thrown during test: " + e.getMessage());
        }
    }

    @Test
    void testProcessResult() {
        try {
            BufferedReader br2 = new BufferedReader(new FileReader("src/test/resources/flightTestData1523701900.json"));
            String newData = br2.readLine();

            // Server should return all 3950 flights (unfiltered)
            server.processResult(server.getData());
            assertEquals(1523701890, server.flightData.timestamp());
            assertEquals(3950, server.flightData.amountFlights());

            // Checking if the old position of a flight is set on next call of processResult
            server.processResult(newData);
            assertEquals(3947, server.flightData.amountFlights());
            assertTrue(server.flightData.flightExists("4853f4"));
            FlightPosition movingFlight = server.flightData.getFlight("4853f4").position();
            assertEquals(movingFlight.previous().getLatitude(), 54.4519, 0.000001);
            assertEquals(movingFlight.previous().getLongitude(), 5.0417, 0.000001);

            // Time didn't progess, so Server should return the last valid data
            server.flightData.setTimestamp(1523799999);
            server.processResult(newData);
            assertEquals(3947, server.flightData.amountFlights());
            assertEquals(3950, server.previousFlightData.amountFlights());
            assertEquals(1523799999, server.flightData.timestamp());

            // Should not throw an exception & data should not change
            server.processResult("these isn't a valid JSON string");
            assertEquals(3947, server.flightData.amountFlights());
        } catch (Exception e) {
            fail("Exception thrown during test: " + e.getMessage());
        }
    }
}
