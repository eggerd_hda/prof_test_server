package businessLogic;

import businesslogic.Flight;
import businesslogic.FlightPosition;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class FlightTest {

    @Test
    void testSetterGetter() {
        Flight flight = new Flight("icao", "mycallsign", new FlightPosition(1, 2, 3, 4), 1234);

        // Getter should return the data, that has been set by the constructor
        assertEquals("icao", flight.getIcao24());
        assertEquals("mycallsign", flight.getCallsign());
        assertEquals(1, flight.position().getLatitude());
        assertEquals(2, flight.position().getLongitude());
        assertEquals(3, flight.position().getHeight());
        assertEquals(4, flight.position().getHeading());
        assertEquals(1234, flight.getLastUpdateTime());

        // Previous position should be added to existing position
        flight.setPrevious(new FlightPosition(991, 992, 993, 994));
        assertEquals(991, flight.position().previous().getLatitude());
        assertEquals(992, flight.position().previous().getLongitude());
        assertEquals(993, flight.position().previous().getHeight());
        assertEquals(994, flight.position().previous().getHeading());
    }

}
