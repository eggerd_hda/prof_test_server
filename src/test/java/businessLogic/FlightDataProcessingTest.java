package businessLogic;

import businesslogic.Flight;
import businesslogic.FlightData;
import businesslogic.FlightFilterValues;
import businesslogic.FlightDataProcessing;
import businesslogic.FlightPosition;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import java.util.Map;
import org.junit.jupiter.api.Test;
import java.io.BufferedReader;
import java.io.FileReader;

import static org.junit.jupiter.api.Assertions.*;

/* The states property is a two-dimensional array. Each row represents a state vector and contains the following fields:
 * Pos  Property 	Type        Description
 * 0 	icao24          string      Unique ICAO 24-bit address of the transponder in hex string representation.
 * 1 	callsign        string      Callsign of the vehicle (8 chars). Can be null if no callsign has been received.
 * 2 	origin_country  string      Country name inferred from the ICAO 24-bit address.
 * 3 	time_position   int         Unix timestamp (seconds) for the last position update. Can be null if no position report was received by OpenSky within the past 15s.
 * 4 	last_contact    int         Unix timestamp (seconds) for the last update in general. This field is updated for any new, valid message received from the transponder.
 * 5 	longitude       float       WGS-84 longitude in decimal degrees. Can be null.
 * 6 	latitude        float       WGS-84 latitude in decimal degrees. Can be null.
 * 7 	geo_altitude    float       Geometric altitude in meters. Can be null.
 * 8 	on_ground       boolean     Boolean value which indicates if the position was retrieved from a surface position report.
 * 9 	velocity        float       Velocity over ground in m/s. Can be null.
 * 10 	heading         float       Heading in decimal degrees clockwise from north (i.e. north=0°). Can be null.
 * 11 	vertical_rate   float       Vertical rate in m/s. A positive value indicates that the airplane is climbing, a negative value indicates that it descends. Can be null.
 * 12 	sensors         int[]       IDs of the receivers which contributed to this state vector. Is null if no filtering for sensor was used in the request.
 * 13 	baro_altitude   float       Barometric altitude in meters. Can be null.
 * 14 	squawk          string      The transponder code aka Squawk. Can be null.
 * 15 	spi             boolean     Whether flight status indicates special purpose indicator.
 * 16 	position_source int         Origin of this state’s position: 0 = ADS-B, 1 = ASTERIX, 2 = MLAT
 */
public class FlightDataProcessingTest {

    Logger log = LoggerFactory.getLogger(FlightDataProcessingTest.class);

    JsonArray sampleFlightData = new JsonArray()
            .add("123456").add("DLH543").add("Germany") // Identification
            .add(1523627920).add(1523627930) // Timestamps
            .add(9.45133f).add(52.1423f).add(731.234).add(false) // Position
            .add(120).add(20.3).add(-50.1f) // Movement
            .addNull().add(750.1).add("4615").add(false).add(0);

    JsonArray sampleFlightData2 = new JsonArray()
            .add("47af16").addNull().add("GreatBritain") // Identification
            .add(1523627930).add(1523627925) // Timestamps
            .add(4.3f).add(47.523f).add(10714.234).add(false) // Position
            .add(240).add(281.3).add(0f) // Movement
            .addNull().add(10650f).add("1000").add(false).add(1);

    JsonArray sampleFlightData3 = new JsonArray()
            .add("3c65c1").add("DLH081  ").add("Germany") // Identification
            .add(1523627930).add(1523627930) // Timestamps
            .add(6.6388).add(51.2193).add(1844.04).add(false) // Position
            .add(118.36).add(202.22).add(15.61) // Movement
            .addNull().add(1836.42).add("1000").add(false).add(0);

    // Everything allowed to be NULL is set to NULL
    JsonArray sampleFlightData4 = new JsonArray()
            .add("2b75c3").addNull().add("Germany") // Identification
            .addNull().add(1523627910) // Timestamps
            .addNull().addNull().addNull().add(false) // Position
            .addNull().addNull().addNull() // Movement
            .addNull().addNull().addNull().add(false).add(0);

    // Longitude invalid
    JsonArray sampleFlightData5 = new JsonArray()
            .add("4a33f5").addNull().add("Germany") // Identification
            .addNull().add(1523627955) // Timestamps
            .add(-180.001f).add(55.123f).add(1337.2f).add(false) // Position
            .addNull().addNull().addNull() // Movement
            .addNull().addNull().addNull().add(false).add(0);

    // Latitude invalid
    JsonArray sampleFlightData6 = new JsonArray()
            .add("4c31f6").addNull().add("Germany") // Identification
            .addNull().add(1523627905) // Timestamps
            .add(44.321f).add(90.001f).add(5344.9f).add(false) // Position
            .addNull().addNull().addNull() // Movement
            .addNull().addNull().addNull().add(false).add(0);

    // Height invalid
    JsonArray sampleFlightData7 = new JsonArray()
            .add("9d71e1").addNull().add("Germany") // Identification
            .addNull().add(1523627905) // Timestamps
            .add(23.341f).add(72.71f).add(-0.001f).add(false) // Position
            .addNull().addNull().addNull() // Movement
            .addNull().addNull().addNull().add(false).add(0);

    // Updated position for sampleFlightData2
    JsonArray sampleFlightData8 = new JsonArray()
            .add("47af16").addNull().add("GreatBritain") // Identification
            .add(1523627935).add(1523627930) // Timestamps
            .add(4.1f).add(48.347f).add(11500.5).add(false) // Position
            .add(240).add(281.3).add(0f) // Movement
            .addNull().add(10650f).add("1000").add(false).add(1);

    // Some "new" flight that appears in timeObject2
    JsonArray sampleFlightData9 = new JsonArray()
            .add("31eb73").addNull().add("Germany") // Identification
            .add(1523627935).add(1523627930) // Timestamps
            .add(4.3f).add(47.523f).add(10714.234).add(false) // Position
            .add(240).add(281.3).add(0f) // Movement
            .addNull().add(10650f).add("1000").add(false).add(1);

    /**
     * The complete state object contains one timestamp and an array of states
     * for each flight-object
     */
    JsonObject timeObject = new JsonObject()
            .put("time", 1523627930)
            .put("states", new JsonArray()
                    .add(sampleFlightData)
                    .add(sampleFlightData2)
                    .add(sampleFlightData3)
                    .add(sampleFlightData4)
                    .add(sampleFlightData5)
                    .add(sampleFlightData6)
                    .add(sampleFlightData7));

    /**
     * Represents new data that was loaded after timeObject
     */
    JsonObject timeObject2 = new JsonObject()
            .put("time", 1523627930)
            .put("states", new JsonArray().add(sampleFlightData8).add(sampleFlightData9));

    @Test
    void testFlightPositionExtraction() {
        // Validates that the expected values for latitude, lonitude & height are returned
        FlightPosition flightPosition = FlightDataProcessing.extractPosition(sampleFlightData);
        assertEquals(sampleFlightData.getFloat(6), flightPosition.getLatitude(), 0.000001);
        assertEquals(sampleFlightData.getFloat(5), flightPosition.getLongitude(), 0.000001);
        assertEquals(sampleFlightData.getFloat(7), flightPosition.getHeight(), 0.000001);
        assertEquals(sampleFlightData.getFloat(10), flightPosition.getHeading(), 0.000001);

        // Longitude, latitude & height can be NULL; should return NULL
        assertNull(FlightDataProcessing.extractPosition(sampleFlightData4));

        // Longitude invalid; should return NULL
        assertNull(FlightDataProcessing.extractPosition(sampleFlightData5));

        // Latitude invalid; should return NULL
        assertNull(FlightDataProcessing.extractPosition(sampleFlightData6));

        // Height invalid; should return NULL
        assertNull(FlightDataProcessing.extractPosition(sampleFlightData7));
    }

    @Test
    void testFilterGeographicPosition() {
        FlightFilterValues filter = new FlightFilterValues().setRegion(47.523f, 52.1423f, 4.3f, 9.45133f);

        // Flight is exactly in a corner of the region (max latitude & longitude)
        assertTrue(FlightDataProcessing.filterGeographicPosition(filter, FlightDataProcessing.extractPosition(sampleFlightData)));

        // Flight is exactly in a corner of the region (min latitude & longitude)
        assertTrue(FlightDataProcessing.filterGeographicPosition(filter, FlightDataProcessing.extractPosition(sampleFlightData2)));

        // Flight is somewhere inside the region
        assertTrue(FlightDataProcessing.filterGeographicPosition(filter, FlightDataProcessing.extractPosition(sampleFlightData3)));

        // Flight not in the region
        assertFalse(FlightDataProcessing.filterGeographicPosition(filter, FlightDataProcessing.extractPosition(sampleFlightData8)));
    }

    @Test
    void testFilterFlightHeight() {
        FlightFilterValues filter = new FlightFilterValues().setHeight(731.234f, 10714.234f);

        // Flight is exactly on the minimum height
        assertTrue(FlightDataProcessing.filterFlightHeight(filter, FlightDataProcessing.extractPosition(sampleFlightData)));

        // Flight is exactly on the maximum height
        assertTrue(FlightDataProcessing.filterFlightHeight(filter, FlightDataProcessing.extractPosition(sampleFlightData2)));

        // Flight is somewhere in the height range
        assertTrue(FlightDataProcessing.filterFlightHeight(filter, FlightDataProcessing.extractPosition(sampleFlightData3)));

        // Flight is too high
        assertFalse(FlightDataProcessing.filterFlightHeight(filter, FlightDataProcessing.extractPosition(sampleFlightData8)));
    }

    @Test
    void testSetPreviousPositions() {
        try {
            FlightData oldData = new FlightData(timeObject.toString());
            FlightData newData = new FlightData(timeObject2.toString());
            FlightDataProcessing.setPreviousPositions(oldData, newData);

            // Map should contain the updated flight
            assertTrue(newData.flightExists("47af16"));

            // Also checking if its previous position has been set correctly
            FlightPosition previousPos = newData.getFlight("47af16").position().previous();
            assertEquals(sampleFlightData2.getFloat(6), previousPos.getLatitude(), 0.000001);
            assertEquals(sampleFlightData2.getFloat(5), previousPos.getLongitude(), 0.000001);
            assertEquals(sampleFlightData2.getFloat(7), previousPos.getHeight(), 0.000001);
            assertEquals(sampleFlightData2.getFloat(10), previousPos.getHeading(), 0.000001);

            // Checking if the new flight exists
            assertTrue(newData.flightExists("31eb73"));

            // Checking if flight without update doesn't exist
            assertFalse(newData.flightExists("123456"));
        } catch (Exception e) {
            fail("Exception thrown during test: " + e.getMessage());
        }
    }

    @Test
    void testSetPreviousPositionsExampleData() {
        try {
            /**
             * Flights that haven't changed position and no new timestamp:
             * 8a02ff, 880456, 4845eb, 44027c, 3c666b, ace6e2, 7c6b3d and more
             *
             * Flights that aren't in the new data set (old flights): 49d2d7,
             * 7804f5, 8005f7, 850d8c, 888040, e48274, a8222c, 780d3f, 780d79
             *
             * Flights that aren't in the old data set (new flights): 851c86,
             * a0a644, a73b7e, a9fc34, a403df, 850e14, a65c29, a9be0e, 345212
             */

            BufferedReader br1 = new BufferedReader(new FileReader("src/test/resources/flightTestData1523701890.json"));
            BufferedReader br2 = new BufferedReader(new FileReader("src/test/resources/flightTestData1523701900.json"));

            FlightData oldData = new FlightData(br1.readLine());
            FlightData newData = new FlightData(br2.readLine());
            FlightDataProcessing.setPreviousPositions(oldData, newData);

            // Map should contain the first flight of both data sets
            assertTrue(newData.flightExists("ab1644"));

            // Map should contain the last flight of both data sets
            assertTrue(newData.flightExists("7808f8"));

            // Checking if the new flight exists
            assertTrue(newData.flightExists("a0a644"));

            // Checking if old flight doesn't exist
            assertFalse(newData.flightExists("49d2d7"));

            // Checking if the old position of a "standing" flight is correct
            assertTrue(newData.flightExists("8a02ff"));
            FlightPosition standingFlight = newData.getFlight("8a02ff").position();
            assertEquals(standingFlight.getLatitude(), standingFlight.previous().getLatitude(), 0.000001);
            assertEquals(standingFlight.getLongitude(), standingFlight.previous().getLongitude(), 0.000001);
            assertEquals(standingFlight.getHeight(), standingFlight.previous().getHeight(), 0.000001);
            assertEquals(standingFlight.getHeading(), standingFlight.previous().getHeading(), 0.000001);

            // Checking if the old position of a moving flight is correct
            assertTrue(newData.flightExists("4853f4"));
            FlightPosition movingFlight = newData.getFlight("4853f4").position();
            assertEquals(movingFlight.previous().getLatitude(), 54.4519, 0.000001);
            assertEquals(movingFlight.previous().getLongitude(), 5.0417, 0.000001);

            // Just counting the amount of flights with a previous position
            int oldPositionCounter = 0;
            for (Map.Entry<String, Flight> entry : newData.entrySet()) {
                if (entry.getValue().position().previous() != null) {
                    oldPositionCounter++;
                }
            }
            log.info("Flights with previous positions: " + oldPositionCounter);
        } catch (Exception e) {
            fail("Exception thrown during test: " + e.getMessage());
        }
    }

    @Test
    void testPerformanceFilteringAndSettingPreviousPosition() {
        try {
            BufferedReader br1 = new BufferedReader(new FileReader("src/test/resources/flightTestData1523701890.json"));
            BufferedReader br2 = new BufferedReader(new FileReader("src/test/resources/flightTestData1523701900.json"));

            FlightData oldData = new FlightData(br1.readLine());
            FlightData newData = new FlightData(br2.readLine());
            FlightData filteredData = new FlightData();
            FlightFilterValues filter = new FlightFilterValues().setRegion(47.523f, 52.1423f, 4.3f, 9.45133f);

            /**
             * The loops are required to prevent just-in-time compilation, which
             * would result in incorrect measurements - the second function call
             * would always be faster than the first, regardless of the order
             *
             * Both variants should roughly need the same amount of time, since
             * setPreviousPositions() always iterates over the full map of
             * oldData and filtering always iterates over the map of newData
             */
            int loops = 10000;
            double startTime = 0;

            // Filter first and set previous positions afterwards            
            for (int i = 0; i <= loops; i++) {
                startTime = filterThenSetPrevious(filter, oldData, newData, filteredData);
            }

            log.info("Time for filtering & then setting previous positions: " + (System.nanoTime() - startTime) / 1000000 + " ms");
            assertEquals(190, filteredData.amountFlights());
            filteredData = new FlightData();

            // Set previous positions first and filter afterwards
            for (int i = 0; i <= loops; i++) {
                startTime = setPreviousThenFilter(filter, oldData, newData, filteredData);
            }

            log.info("Time for setting previous positions & then filtering: " + (System.nanoTime() - startTime) / 1000000 + " ms");
            log.info("Flights inside the geographic region: " + filteredData.amountFlights());
            assertEquals(190, filteredData.amountFlights());
        } catch (Exception e) {
            fail("Exception thrown during test: " + e.getMessage());
        }
    }

    /**
     * Helper for testPerformanceFilteringAndSettingPreviousPosition()
     */
    long filterThenSetPrevious(FlightFilterValues filter, FlightData oldData, FlightData newData, FlightData filteredData) {
        long startTime = System.nanoTime();

        for (Map.Entry<String, Flight> entry : newData.entrySet()) {
            if (FlightDataProcessing.filterGeographicPosition(filter, entry.getValue().position())) {
                filteredData.addFlight(entry.getKey(), entry.getValue());
            }
        }
        FlightDataProcessing.setPreviousPositions(oldData, filteredData);

        return startTime;
    }

    /**
     * Helper for testPerformanceFilteringAndSettingPreviousPosition()
     */
    long setPreviousThenFilter(FlightFilterValues filter, FlightData oldData, FlightData newData, FlightData filteredData) {
        long startTime = System.nanoTime();

        FlightDataProcessing.setPreviousPositions(oldData, newData);
        for (Map.Entry<String, Flight> entry : newData.entrySet()) {
            if (FlightDataProcessing.filterGeographicPosition(filter, entry.getValue().position())) {
                filteredData.addFlight(entry.getKey(), entry.getValue());
            }
        }

        return startTime;
    }
}
