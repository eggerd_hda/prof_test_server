package businessLogic;

import businesslogic.FlightFilterValues;
import io.vertx.core.json.JsonObject;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class FlightFilterValuesTest {

    @Test
    void testDefaultFilter() {
        FlightFilterValues filter = new FlightFilterValues();

        // Default filter should have maximum latitude range
        assertEquals(-90, filter.getLatitudes()[0]);
        assertEquals(90, filter.getLatitudes()[1]);

        // Default filter should have maximum longitude range
        assertEquals(-180, filter.getLongitudes()[0]);
        assertEquals(180, filter.getLongitudes()[1]);

        // Default filter should go from ground to maximum flight level
        assertEquals(0, filter.getHeights()[0]);
        assertEquals(20000, filter.getHeights()[1]);
    }

    @Test
    void testSetterGetter() {
        // Filter should accept invalid latitude & longitude coordinates
        FlightFilterValues filter = new FlightFilterValues().setRegion(91, -22, 181, -55).setHeight(99, -5);

        // Latitude should be sorted when returned
        assertEquals(-22, filter.getLatitudes()[0]);
        assertEquals(91, filter.getLatitudes()[1]);

        // Longiute should be sorted when returned
        assertEquals(-55, filter.getLongitudes()[0]);
        assertEquals(181, filter.getLongitudes()[1]);

        // Height should be sorted when returned
        assertEquals(-5, filter.getHeights()[0]);
        assertEquals(99, filter.getHeights()[1]);
    }

    @Test
    void testRegionToJson() {
        try {
            FlightFilterValues filter = new FlightFilterValues().setRegion(80, -5, 121, -18).setHeight(50, -7);
            JsonObject string = new JsonObject(filter.regionToJson());

            // Validating Latitude coordinates
            assertTrue(string.containsKey("lat1"));
            assertTrue(string.containsKey("lat2"));
            assertEquals(filter.getLatitudes()[0], string.getFloat("lat1"), 0.00001);
            assertEquals(filter.getLatitudes()[1], string.getFloat("lat2"), 0.00001);

            // Validating Longitude coordinates
            assertTrue(string.containsKey("lon1"));
            assertTrue(string.containsKey("lon2"));
            assertEquals(filter.getLongitudes()[0], string.getFloat("lon1"), 0.00001);
            assertEquals(filter.getLongitudes()[1], string.getFloat("lon2"), 0.00001);

            // String should not contain the filter height
            assertFalse(string.containsKey("hei1"));
            assertFalse(string.containsKey("hei2"));
        } catch (Exception e) {
            fail("Exception thrown during test: " + e.getMessage());
        }
    }
}
