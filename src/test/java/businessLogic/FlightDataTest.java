package businessLogic;

import businesslogic.Flight;
import businesslogic.FlightData;
import businesslogic.FlightPosition;

import io.vertx.core.json.DecodeException;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import java.io.BufferedReader;
import java.io.FileReader;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class FlightDataTest {

    @Test
    void testConstructorInvalidJson() {
        // Passing a invalid JSON string as parameter should throw an exception
        assertThrows(DecodeException.class, () -> {
            FlightData flights = new FlightData("This isn't valid Json");
        });

        // Using a JSON string without a timestamp should throw an exception
        assertThrows(Exception.class, () -> {
            FlightData flights = new FlightData("{\"states\":[[\"ab1644\",\"UAL2139 \",\"United States\",1523701889,1523701889,-80.0049,42.4682,10972.8,false,196.06,274.36,0.33,null,11216.64,\"4512\",false,0]]}");
        });

        // Using a JSON string without a state array should throw an exception
        assertThrows(Exception.class, () -> {
            FlightData flights = new FlightData("{\"time\":1523701890}");
        });
    }

    @Test
    void testSetterGetter() {
        try {
            BufferedReader br = new BufferedReader(new FileReader("src/test/resources/flightTestData1523701890.json"));
            FlightData flights = new FlightData(br.readLine());

            // Checking if all 3950 flights with valid positions have been mapped
            assertEquals(3950, flights.amountFlights());

            // Timestamp should be the same as in the JSON string
            assertEquals(1523701890, flights.timestamp());

            // setTimestamp should overwrite the timestamp from the JSON string
            flights.setTimestamp(1234);
            assertEquals(1234, flights.timestamp());

            // Flights with valid position should be present in the map
            assertTrue(flights.flightExists("ab1644"));
            assertTrue(flights.flightExists("405ee0"));
            assertTrue(flights.flightExists("7808f8"));

            // Flight with invalid position should not be present in the map
            assertFalse(flights.flightExists("4b1801"));
            assertFalse(flights.flightExists("e80211"));
            assertFalse(flights.flightExists("4b1902"));

            // Validating the data of a flight
            Flight f = flights.getFlight("7808f8");
            assertEquals("7808f8", f.getIcao24());
            assertEquals("B8251   ", f.getCallsign());
            assertEquals(1523701888, f.getLastUpdateTime());
            assertEquals(126.9899, f.position().getLongitude(), 0.00001);
            assertEquals(36.2614, f.position().getLatitude(), 0.00001);
            assertEquals(5151.12, f.position().getHeight(), 0.001);
            assertEquals(5.11, f.position().getHeading(), 0.001);

            // Trying to get non-existing flight should return NULL
            assertNull(flights.getFlight("39850e"));

            // Manually adding a new flight to the map & validating the data
            flights.addFlight("myicao", new Flight("myicao", "mycallsign", new FlightPosition(1, 2, 3, 4), 1234));
            f = flights.getFlight("myicao");
            assertEquals("myicao", f.getIcao24());
            assertEquals("mycallsign", f.getCallsign());
            assertEquals(1234, f.getLastUpdateTime());
            assertEquals(1, f.position().getLatitude());
            assertEquals(2, f.position().getLongitude());
            assertEquals(3, f.position().getHeight());
            assertEquals(4, f.position().getHeading());
        } catch (Exception e) {
            fail("Exception thrown during test: " + e.getMessage());
        }
    }

    @Test
    void testToJson() {
        try {
            BufferedReader br = new BufferedReader(new FileReader("src/test/resources/flightTestData1523701890.json"));
            FlightData flights = new FlightData(br.readLine());

            // Converting to string & then back into a object to test its content
            JsonObject string = new JsonObject(flights.toJson());

            // String should contain the same timestamp
            assertTrue(string.containsKey("time"));
            assertEquals(flights.timestamp(), (int) string.getInteger("time"));

            // String should contain a list of flights
            assertTrue(string.containsKey("flights"));
            JsonArray stringFlights = string.getJsonArray("flights");

            // Should contain the same amount of flights
            assertEquals(flights.amountFlights(), stringFlights.size());

            // Checking that the string contains all required information to a flight
            JsonObject stringFlightData = stringFlights.getJsonObject(stringFlights.size() - 1);
            assertTrue(stringFlightData.containsKey("icao24"));
            assertTrue(stringFlightData.containsKey("latitude"));
            assertTrue(stringFlightData.containsKey("longitude"));
            assertTrue(stringFlightData.containsKey("height"));
            assertTrue(stringFlightData.containsKey("callsign"));
            assertTrue(stringFlightData.containsKey("heading"));
            assertTrue(stringFlightData.containsKey("lastUpdate"));

            // Validating the data of a flight
            Flight flightOfString = flights.getFlight(stringFlightData.getString("icao24"));
            assertEquals(flightOfString.getIcao24(), stringFlightData.getString("icao24"));
            assertEquals(flightOfString.position().getLatitude(), stringFlightData.getFloat("latitude"), 0.000001);
            assertEquals(flightOfString.position().getLongitude(), stringFlightData.getFloat("longitude"), 0.000001);
            assertEquals(flightOfString.position().getHeight(), stringFlightData.getFloat("height"), 0.000001);
            assertEquals(flightOfString.position().getHeading(), stringFlightData.getFloat("heading"), 0.000001);
            assertEquals(flightOfString.getCallsign(), stringFlightData.getString("callsign"));
            assertEquals(flightOfString.getLastUpdateTime(), (int) stringFlightData.getInteger("lastUpdate"));
        } catch (Exception e) {
            fail("Exception thrown during test: " + e.getMessage());
        }
    }

    @Test
    void testDefaultConstructor() {
        FlightData flights = new FlightData();
        assertEquals(0, flights.amountFlights());
        assertEquals(0, flights.timestamp());
    }
}
