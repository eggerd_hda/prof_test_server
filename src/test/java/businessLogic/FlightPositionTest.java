package businessLogic;

import businesslogic.FlightPosition;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class FlightPositionTest {

    @Test
    void testSetterGetter() {
        // FlightPosition should accept invalid values
        FlightPosition position = new FlightPosition(-91, 181, -1, 361);

        assertEquals(-91, position.getLatitude());
        assertEquals(181, position.getLongitude());
        assertEquals(-1, position.getHeight());
        assertEquals(361, position.getHeading());

        // No previous position set yet
        assertNull(position.previous());

        position.setPrevious(new FlightPosition(1, 2, 3, 4));
        assertEquals(1, position.previous().getLatitude());
        assertEquals(2, position.previous().getLongitude());
        assertEquals(3, position.previous().getHeight());
        assertEquals(4, position.previous().getHeading());
    }
}
